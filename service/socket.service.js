var FriendModel = require('./../models/friend.model');
var waitForYouModel = require('./../models/waitForYou.model');
var MessageModel = require('./../models/message.model');
var message = require('./../utils/message');
var jwt = require('./../utils/jwt');
var User = require('./../models/user.model');

var gcm = require('node-gcm');
var sender = new gcm.Sender('AAAAKT7EJ48:APA91bGfiK2sG26r0oBXRVs8O6UCyS9w_rdah33NW43efBjshZkVnRNTvdnU6hLDwFNvMODbBWonGKgWsaKvZFRzMZgY3s5SdnSm9lbLKp78diNOBpHOeCdQeAABZBmFGbKG9Oi0frP2');

module.exports = {
    makeFriend: makeFriend,
    acceptUserMakeFriend: acceptUserMakeFriend,
    sendMessage: sendMessage
}

function sendMessage(request) {
    let recipients = [];
    var recived = {
        idFriend: request.idFriend,
        idme: request.idme,
        dateSend: request.dateSend,
        message: request.message,
        image: request.image,
        name: request.name
    }
    const messageNotification = gcm.Message({
        data: {
            hello: 'world'
        },
        notification: {
            title: request.name,
            body: request.message,
            click_action: 'http://localhost:4200/home/messages/' + request.idFriend,
            icon: 'http://localhost:3000/static/' + recived.image
        }
    });
    return new Promise((resovle, reject) => {
        var message = new MessageModel({
            fromUserSendID: recived.idme,
            toUserIsRecivedID: recived.idFriend,
            message: recived.message,
            dateSend: recived.dateSend
        });
        message.save((err, message2) => {
            if (err) {
                reject(err);
            } else {
                User.findOne({ _id: recived.idFriend }, (err, userModel) => {
                    if (userModel) {
                        userModel.tokenBrower.forEach(element => {
                            recipients.push(element.idBrowser);
                        });
                        sender.send(messageNotification, { registrationTokens: recipients }, (err, resp) => {
                            console.log(err ? err : resp);
                        });
                    }
                });
                resovle(message2);
            }
        });
    });
}

function acceptUserMakeFriend(request) {
    var recived = {
        idusermakeFriend: request.idusermakeFriend,
        token: request.token
    }
    return new Promise((resovle, reject) => {
        jwt.verify(recived.token, (err, userModel) => {
            if (err) {
                reject(err)
            } else {
                FriendModel.updateOne({ ownerID: userModel._id }, {
                    $push: {
                        listFriend: {
                            friendID: recived.idusermakeFriend
                        }
                    }
                }, (err2, success) => {
                    FriendModel.updateOne({ ownerID: recived.idusermakeFriend }, {
                        $push: {
                            listFriend: {
                                friendID: userModel._id
                            }
                        }
                    }, (err1, success1) => {
                        waitForYouModel.updateOne({ ownerID: userModel._id }, {
                            $pull: {
                                listWaitFriend:
                                    { userMakeFriendID: recived.idusermakeFriend }
                            }
                        }, (err2, success2) => {
                        });
                    });
                    resovle('TO_BE_ACCEPT');
                })
            }
        })
    });
}

function makeFriend(request) {
    var recived = {
        sendRequestMakeFriendId: request.sendRequestMakeFriendId,
        idNeedMakeFriend: request.idNeedMakeFriend
    }

    return new Promise((resovle, reject) => {
        if (recived.sendRequestMakeFriendId && recived.idNeedMakeFriend) {
            waitForYouModel.findOne({ ownerID: recived.idNeedMakeFriend }, (err, result) => {
                if (err) {
                    reject(err);
                } else if (!result) {
                    reject(message.ERROR_MESSAGE.USER.NOT_FOUND_USER);
                } else {
                    if (result.listWaitFriend.contains(recived.sendRequestMakeFriendId)) {
                        reject(message.ERROR_MESSAGE.USER.USER_IS_WAITED_FRIEND);
                    } else {
                        result.listWaitFriend.push({
                            userMakeFriendID: recived.sendRequestMakeFriendId
                        });
                        resovle(result.listWaitFriend);
                        result.save();
                    }
                }

            });
        }
    });
}

Array.prototype.contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}