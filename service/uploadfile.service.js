var uuid = require('uuid');
var config = require('./../utils/message');
var configServer = require('../config');
var fs = require('fs');
var User = require('./../models/user.model');
var Image = require('./../models/personalphoto.model');
var sharp = require('sharp');
var message = require('./../utils/message');

module.exports = {
    uploadFile: uploadFile,
    uploadImageStorage: uploadImageStorage,
    getImageStorageByID: getImageStorageByID
}

function getImageStorageByID(request) {
    var recived = {
        id: request.id,
        skipnumber: request.skipnumber
    };
    return new Promise((resolve, reject) => {
        Image.findOne({ ownerID: recived.id }, (err, imageModel) => {
            if (err) {
                reject({ err: err });
            } else if(imageModel.imageStoarge.length > 0) {
                resolve({ imageModel: imageModel.imageStoarge });
            }else{
                reject({
                    err: message.ERROR_MESSAGE.FILE.DONT_HAVE_IMAGE_IN_STORAGE
                })
            }
        }).skip(recived.skipnumber).limit(20);
    });
}

function uploadImageStorage(request) {
    var id = request.id;
    var base64Data = request.base64Data;
    var typeImg = request.typeImg;
    var type = request.type;
    var nameImg = uuid.v4();
    var height = request.height;
    var width = request.width;
    return new Promise((resolve, reject) => {
        try {
            Image.findOne({ ownerID: id })
                .exec((err, imageModel) => {

                    if (err) reject({
                        message: config.CAN_NOT_UPLOAD
                    })
                    if (!imageModel) {
                        reject({
                            message: config.CAN_NOT_UPLOAD
                        });
                    } else {
                        let url;
                        if (type === '.png' || type === '.jpg' || type === '.jpeg' || type === '.JPG' || type === '.x-icon') {
                            var binaryData = new Buffer(base64Data, 'base64');
                            sharp(binaryData).resize(width, height)
                                .toFile('./public/imagestore/' + nameImg + type)
                                .then(data => {
                                    url = nameImg + type;
                                    imageModel.imageStoarge.push(url);
                                    imageModel.save();
                                    resolve(url);
                                }).catch(err => {
                                    console.log(err);
                                });
                        }
                    }
                });
        } catch (error) {
            reject(error);
        }
    })

}

function uploadFile(request) {
    var id = request.id;
    var base64Data = request.base64Data;
    var typeImg = request.typeImg;
    var type = request.type;
    var nameImg = uuid.v4();
    var height = request.height;
    var width = request.width;
    return new Promise((resolve, reject) => {
        try {
            User.findById({ _id: id })
                .exec((err, userModel) => {

                    if (err) reject({
                        message: config.CAN_NOT_UPLOAD
                    })
                    if (!userModel) {
                        reject({
                            message: config.CAN_NOT_UPLOAD
                        });
                    } else {
                        let url;
                        if (type === '.png' || type === '.jpg' || type === '.jpeg' || type === '.JPG' || type === '.x-icon') {
                            var binaryData = new Buffer(base64Data, 'base64');
                            sharp(binaryData).resize(width, height)
                                .toFile('./public/avatar/' + nameImg + type)
                                .then(data => {
                                    url = nameImg + type;
                                    if (userModel.image) {
                                        fs.unlink('public/avatar/' + userModel.image);
                                    }
                                    userModel.image = url || userModel.image;
                                    userModel.save();
                                    resolve(url);
                                }).catch(err => {
                                    console.log(err);
                                });
                        } else if (type === '.pdf') {
                            var binaryData = new Buffer(base64Data, 'base64').toString('binary');//Read File You Send To
                            fs.writeFile('public/pdf/' + nameImg + type, binaryData, "binary", function (err) {
                                url = configServer.DOMAIN + ':' + configServer.PORT + '/static/' + nameImg + type;
                                resolve(url);
                            });
                        } else {
                            var binaryData = new Buffer(base64Data, 'base64').toString('utf8');//Read File You Send To
                            fs.writeFile('public/text/' + nameImg + type, binaryData, "utf8", function (err) {
                                url = configServer.DOMAIN + ':' + configServer.PORT + '/static/' + nameImg + type;
                                resolve(url);
                            });
                        }
                    }
                });
        } catch (error) {
            reject(error);
        }
    })
}