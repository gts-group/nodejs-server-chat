var crypto = require('./../utils/crypto');
var message = require('./../utils/message');
var jwt = require('./../utils/jwt');
var waitForYouModel = require('./../models/waitForYou.model');
var friendModel = require('./../models/friend.model');
var messageModel = require('./../models/message.model');
var User = require('./../models/user.model');
module.exports = {
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUser: updateUser,
    deleteUser: deleteUser,
    getUserByEmail: getUserByEmail,
    createUser: createUser,
    searchUserArray: searchUserArray,
    checkStateUser: checkStateUser,
    checkNotification: checkNotification,
    getArrayUserByArrayId: getArrayUserByArrayId,
    cancleFriend: cancleFriend,
    checkTokenBrower: checkTokenBrower,
    getMessage: getMessage
}

function checkTokenBrower(request) {
    var recived = {
        idme: request.idme,
        tokenBrower: request.tokenBrower
    }
    let check = 0;
    return new Promise((resolve, reject) => {
        User.findOne({ '_id': recived.idme }, function (err, person) {
            if (person) {
                person.tokenBrower.forEach(element => {
                    if (element.idBrowser === recived.tokenBrower) {
                        check++;
                    }
                });
                if (check === 0) {
                    User.updateOne({ '_id': recived.idme }, {
                        $push: {
                            tokenBrower:
                                { idBrowser: recived.tokenBrower }
                        }
                    }, function (err, person) {
                        if (!err) {
                            resolve('You Are Provider !');
                        }
                    });
                }
            } else {
                reject(err);
            }
        });
    });
}

function getMessage(request) {
    var request = {
        idother: request.idother,
        idme: request.idme,
        numericalOrder: request.numericalOrder
    };
    return new Promise((resolve, reject) => {
        var message = [];
        messageModel.find({ fromUserSendID: request.idother, toUserIsRecivedID: request.idme }, (err, messages1) => {
            if (err) {
                reject(err);
            } else {
                message = message.concat(messages1);
                messageModel.find({ fromUserSendID: request.idme, toUserIsRecivedID: request.idother }, (err1, messages2) => {
                    if (err1) {
                        reject(err1);
                    } else {
                        message = message.concat(messages2);
                        var messageTemp;
                        for (let i = 0; i < message.length; i++) {
                            for (let j = i + 1; j < message.length; j++) {
                                if (message[i].dateSend < message[j].dateSend) {
                                    messageTemp = message[j];
                                    message[j] = message[i];
                                    message[i] = messageTemp
                                }
                            }
                        }
                        resolve(message);
                    }
                }).skip(request.numericalOrder).limit(10).sort({ dateSend: -1 });
            }
        }).skip(request.numericalOrder).limit(10).sort({ dateSend: -1 });
    });
}

function cancleFriend(request) {
    var recived = {
        idother: request.idother,
        idme: request.idme
    }
    return new Promise((resolve, reject) => {
        friendModel.updateOne({ ownerID: recived.idme }, {
            $pull: {
                listFriend: {
                    friendID: recived.idother
                }
            }
        }, (err1, success1) => {
            if (err1) {
                reject(err1);
            }
        });
        friendModel.updateOne({ ownerID: recived.idother }, {
            $pull: {
                listFriend: {
                    friendID: recived.idme
                }
            }
        }, (err2, success2) => {
            if (err2) {
                reject(err2);
            }
        });
        resolve('CANCLE_SUCCESS');
    });
}

function getArrayUserByArrayId(arrayUserID) {
    var arrayUserID = arrayUserID;
    return new Promise((resolve, reject) => {
        var userArray = [];
        for (var i = 0; i < arrayUserID.length; i++) {
            console.log(arrayUserID[i]);
            User.findOne({ _id: arrayUserID[i].userMakeFriendID }, (err, userModel) => {
                if (err) {
                    reject(err);
                } else {
                    userArray.push(userModel);
                    if (userArray.length === arrayUserID.length) {
                        resolve(userArray);
                    }
                }
            });
        }
    });
}

function checkNotification(idme) {
    return new Promise((resolve, reject) => {
        waitForYouModel.findOne({ ownerID: idme }, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result.listWaitFriend);
            }
        });
    });
}

function checkStateUser(idother, idme) {
    return new Promise((resolve, reject) => {
        friendModel.findOne({ ownerID: idme }, (err, friendModel) => {
            if (err) {
                reject(err);
            } else if (friendModel.listFriend.containsFriend({ friendID: idother })) {
                resolve({ check: message.ERROR_MESSAGE.USER.USER_IS_FRIEND })
            } else {
                waitForYouModel.findOne({ ownerID: idother }, (err1, waitForYouModel1) => {
                    if (err1) {
                        reject(err);
                    } else if (waitForYouModel1.listWaitFriend.containsWFriend({ userMakeFriendID: idme })) {
                        resolve({ check: message.ERROR_MESSAGE.USER.I_M_MAKE_FRIEND_WITH_THAT_PERSON })
                    } else {
                        waitForYouModel.findOne({ ownerID: idme }, (err2, waitForYouModel2) => {
                            if (err2) {
                                reject(err2);
                            } else if (waitForYouModel2.listWaitFriend.containsWFriend({ userMakeFriendID: idother })) {
                                resolve({ check: message.ERROR_MESSAGE.USER.USER_IS_WAITED_ACCEPT });
                            } else {
                                resolve({ check: message.ERROR_MESSAGE.USER.USER_DO_NOT_MAKE_FRIEND });
                            }
                        });
                    }
                })
            }
        });
    });
}


function searchUserArray(request) {
    var search = {
        textSearch: request.textSearch,
        id: request.id
    }
    return new Promise((resolve, reject) => {
        try {
            User.find({ fullname: { $regex: search.textSearch }, _id: { $not: { $eq: search.id } } }).exec((err, userModelArray) => {
                if (err) {
                    reject(message.ERROR_MESSAGE.USER.NOT_FOUND_USER);
                } else if (userModelArray) {
                    resolve(userModelArray);
                } else {
                    reject(message.STATUS_CODE.NOT_FOUND);
                }
            });
        } catch (error) {
            reject(message.STATUS_CODE.NOT_FOUND);
        }
    });
}




function getUserByEmail(email) {
    return new Promise((resolve, reject) => {
        User.findOne({
            email: email
        }).exec(function (err, response) {
            if (err) {
                reject({
                    message: err.message
                });
            } else {
                resolve(response);
            }
        });
    })
}
function deleteUser(request) {
    return new Promise((resolve, reject) => {
        User.findOne({
            _id: request.id
        }).exec(function (err, response) {
            if (err) {
                reject({
                    message: err.message
                });
            } else {
                if (!response) {
                    reject({
                        statusCode: message.STATUS_CODE.NOT_FOUND,
                        message: message.ERROR_MESSAGE.USER.NOT_FOUND
                    })
                } else {
                    User.remove({
                        _id: request.id
                    }).exec(function (err, response) {
                        if (err) {
                            reject({
                                statusCode: message.STATUS_CODE.NOT_FOUND,
                                message: message.ERROR_MESSAGE.USER.NOT_FOUND
                            });
                        } else {
                            resolve({
                                statusCode: message.STATUS_CODE.SUCCES,
                                message: message.SUCCESS_MESSAGE.USER.DELETED
                            });
                        }
                    });
                }
            }
        });
    });
}
function updateUser(request) {
    return new Promise((resolve, reject) => {
        User.findOne({
            _id: request.id
        }).exec(function (err, userModel) {
            if (err) {
                reject({
                    message: message.ERROR_MESSAGE.USER.NOT_FOUND
                });
            } else {
                if (userModel) {
                    userModel.firstname = request.firstname || userModel.firstname;
                    userModel.lastname = request.lastname || userModel.lastname;
                    userModel.email = request.email || userModel.email;
                    userModel.age = request.age || userModel.age;
                    userModel.company = request.company || userModel.company;
                    userModel.slogan = request.slogan || userModel.slogan;
                    userModel.address = request.address || userModel.address;
                    userModel.sex = request.sex || userModel.sex;
                    userModel.job = request.job || userModel.job;
                    userModel.favorite = request.favorite || userModel.favorite;
                    userModel.password = request.password ? crypto.hashWithSalt(request.password, userModel.salt) : undefined || userModel.password;
                    userModel.save(function (err, response) {
                        if (err) {
                            reject({
                                message: message.ERROR_MESSAGE.USER.NOT_FOUND
                            })
                        } else {
                            jwt.sign(convertUserModelToUserResponse(response), function (err, token) {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve({
                                        token: token
                                    })
                                }
                            });
                        }
                    });
                } else {
                    reject({
                        statusCode: message.STATUS_CODE.NOT_FOUND,
                        message: message.ERROR_MESSAGE.USER.NOT_FOUND
                    });
                }
            }
        })
    });
}
function getUserById(id) {
    return new Promise((resolve, reject) => {
        User.findOne({
            _id: id
        }).exec(function (err, response) {
            if (err) {
                reject({
                    message: err.message
                });
            } else {
                if (!response) {
                    reject({
                        statusCode: message.STATUS_CODE.NOT_FOUND,
                        message: message.ERROR_MESSAGE.NOT_FOUND
                    });
                } else {
                    resolve(response)
                }
            }
        });
    });
}
function getAllUser() {
    return new Promise((resolve, reject) => {
        User.find({}).exec(function (err, response) {
            if (err) {
                reject(err)
            } else {
                resolve(response);
            }
        })
    });
}
function createUser(request) {
    return new Promise((resolve, reject) => {
        User.findOne({
            email: request.email
        }).exec(function (err, userModel) {
            if (err) {
                reject(err);
            } else {
                if (!userModel) {
                    var salt = crypto.genSalt();
                    var newUser = new User({
                        email: request.email,
                        password: request.password,
                        firstname: request.firstname,
                        lastname: request.lastname,
                        address: request.address,
                        companyschool: request.companyschool,
                        sex: request.sex,
                        age: request.age,
                        salt: salt,
                        favorite: request.favorite,
                        job: request.job,
                        slogan: request.slogan,
                        fullname: request.lastname + ' ' + request.firstname,
                        password: crypto.hashWithSalt(request.password, salt),
                        image: "",
                        tokenBrower: [],
                        modifiDate: new Date(),
                        createdDate: new Date(),
                        deleted: 0
                    });

                    newUser.save(function (err, response) {
                        if (err) {
                            reject(err);
                        } else {
                            var newWaitForYouModel = new waitForYouModel({
                                ownerID: response._id,
                                listWaitFriend: []
                            });
                            var newFriendModel = new friendModel({
                                ownerID: response._id,
                                listFriend: []
                            })
                            newWaitForYouModel.save();
                            newFriendModel.save();
                            jwt.sign(convertUserModelToUserResponse(response), function (err, token) {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve({
                                        token: token
                                    })
                                }
                            })
                        }
                    });
                } else {
                    reject({
                        statusCode: message.STATUS_CODE.NOT_FOUND,
                        message: message.ERROR_MESSAGE.USER.EMAIL_EXIST
                    });
                }
            }
        });
    });
}

function convertUserModelToUserResponse(userModel) {
    var userObj = userModel.toObject();
    delete userObj.password;
    delete userObj.salt;
    delete userObj.createdDate;
    delete userObj.deleted;
    delete userObj.modifiDate;
    delete userObj.fullname;
    return userObj;
}


Array.prototype.containsFriend = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i].friendID === obj.friendID) {
            return true;
        }
    }
    return false;
}

Array.prototype.containsWFriend = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i].userMakeFriendID === obj.userMakeFriendID) {
            return true;
        }
    }
    return false;
}