var uploadfileService = require('./../service/uploadfile.service');

module.exports = {
    uploadAvatar: uploadAvatar,
    uploadImageStorage: uploadImageStorage,
    getImageStorageByID: getImageStorageByID

}
function getImageStorageByID(req, res) {
    var request = {
        id: req.user._id,
        skipnumber: req.body.skipnumber
    }
    uploadfileService.getImageStorageByID(request)
        .then(response => {
            res.send(response);
        }).catch(err => {
            res.send(err);
        });
}

function uploadImageStorage(req, res) {
    var id = req.params.id;//ID User is Logining
    var base64Data = req.body.image.replace(/^data:(image|application|text)\/(x-icon|png|jpeg|json|plain|pdf);base64,/, "");//Cut Part Don't Need
    var typeImg = req.body.name;//Name Real Image
    var width = req.body.width;
    var height = req.body.height
    var type = typeImg.substring(typeImg.indexOf("."));// type of your Profit

    var request = {
        id: id,
        base64Data: base64Data,
        typeImg: typeImg,
        type: type,
        width: width,
        height: height
    }

    uploadfileService.uploadImageStorage(request).then((response) => {
        res.send({ url: response });
    })
        .catch((err) => {
            res.send(err);
        });

}

function uploadAvatar(req, res) {
    var id = req.params.id;//ID User is Logining
    var base64Data = req.body.image.replace(/^data:(image|application|text)\/(x-icon|png|jpeg|json|plain|pdf);base64,/, "");//Cut Part Don't Need
    var typeImg = req.body.name;//Name Real Image
    var width = req.body.width;
    var height = req.body.height
    var type = typeImg.substring(typeImg.indexOf("."));// type of your Profit

    var request = {
        id: id,
        base64Data: base64Data,
        typeImg: typeImg,
        type: type,
        width: width,
        height: height
    }

    uploadfileService.uploadFile(request).then((response) => {
        res.send({ url: response });
    })
        .catch((err) => {
            res.send(err);
        });
}