var userService = require('./../service/user.service');
var message = require('./../utils/message');
var fs = require('fs');
var path = require('path');
module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUser: updateUser,
    deleteUser: deleteUser,
    getUserByEmail: getUserByEmail,
    searchUserArray: searchUserArray,
    checkStateUser: checkStateUser,
    checkNotification: checkNotification,
    getArrayUserByArrayId: getArrayUserByArrayId,
    cancleFriend: cancleFriend,
    getMessage: getMessage,
    checkTokenBrower: checkTokenBrower
}

function checkTokenBrower(req, res) {
    var request = {
        idme: req.body.idme,
        tokenBrower: req.body.tokenBrower
    }
    userService.checkTokenBrower(request).then(result => {
        res.send(result);
    }).catch(err => {
        res.send(err);
    })
}

function getMessage(req, res) {
    var request = {
        idother: req.body.idother,
        idme: req.body.idme,
        numericalOrder: req.body.numericalOrder
    };
    userService.getMessage(request).then(result => {
        res.send(result);
    }).catch(err => {
        res.send(err);
    });
}

function cancleFriend(req, res) {
    var request = {
        idother: req.params.idother,
        idme: req.params.idme
    }
    userService.cancleFriend(request).then(result => {
        res.send(result);
    }).catch(err => {
        res.send(err);
    });
}

function getArrayUserByArrayId(req, res) {
    var arrayUserID = req.body.arrayID;
    userService.getArrayUserByArrayId(arrayUserID).then(result => {
        res.send(result)
    }).catch(err => {
        res.send(err);
    });
}

function checkNotification(req, res) {
    var idme = req.params.idme;
    userService.checkNotification(idme).then(result => {
        res.send(result)
    }).catch(err => {
        res.send(err);
    });
}

function checkStateUser(req, res) {
    var idother = req.params.idother;
    var idme = req.params.idme;
    userService.checkStateUser(idother, idme).then(result => {
        res.send(result);
    }).catch(err => {
        res.send(err);
    })
}

function searchUserArray(req, res) {
    var textSearch = {
        textSearch: req.body.keySearch,
        id: req.body.id
    };
    userService.searchUserArray(textSearch).then(result => {
        var response = {
            result: result
        }
        res.send(response);
    }).catch(err => {
        var response = {
            err: err
        }
        res.send(response);
    });
}

function getUserByEmail(req, res) {
    var email = req.params.email;
    userService.getUserByEmail(email)
        .then(function (response) {
            res.send(response);
        })
        .catch(function (err) {
            res.send(err);
        });
};

function deleteUser(req, res) {
    var request = {
        id: req.params.id
    }
    userService.deleteUser(request).then(function (response) {
        res.send(response)
    }).catch(function (err) {
        res.send(err)
    });
}

function updateUser(req, res) {
    var request = {
        id: req.body.id,
        email: req.body.email,
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        address: req.body.address,
        companyschool: req.body.companyschool,
        sex: req.body.sex,
        age: req.body.age,
        favorite: req.body.favorite,
        job: req.body.job,
        slogan: req.body.slogan
    };


    userService.updateUser(request).then(function (response) {
        res.send(response);
    }).catch(function (err) {
        res.send(err)
    });
}
function getUserById(req, res) {
    var id = req.params.id;
    userService.getUserById(id).then(function (response) {
        res.send(response)
    }).catch(function (err) {
        res.send(err)
    });
}
function getAllUser(req, res) {
    userService.getAllUser().then(function (response) {
        res.send(response);
    }).catch(function (err) {
        res.send(err)
    })
}

function createUser(req, res) {
    var request = {
        email: req.body.email,
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        address: req.body.address,
        companyschool: req.body.companyschool,
        sex: req.body.sex,
        age: req.body.age,
        favorite: req.body.favorite,
        job: req.body.job,
        slogan: req.body.slogan
    };
    userService.createUser(request).then(function (response) {
        res.send(response)
    }).catch(function (err) {
        res.send(err)
    });
}



