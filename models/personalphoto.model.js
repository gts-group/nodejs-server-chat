const mongoose = require('mongoose');
const config = require('./../config');

var Schema = mongoose.Schema;

const personalphotoSchema = new Schema({
    ownerID: {
        type: String,
        required: true
    },
    imageStoarge: {
        type: []
    }
});

const PersonalPhotoSchema = mongoose.model('personalphoto', personalphotoSchema);
module.exports = PersonalPhotoSchema;