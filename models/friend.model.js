const mongoose = require('mongoose');
const config = require('./../config');

var Schema = mongoose.Schema;

const FriendSchema = new Schema({
    ownerID: {
        type: Schema.Types.ObjectId,
        ref: "user"
    },
    listFriend: {
        type: []
    }
});

const friendSchema = mongoose.model('friend', FriendSchema);

module.exports = friendSchema;