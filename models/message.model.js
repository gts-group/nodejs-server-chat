const mongoose = require('mongoose');
const config = require('./../config');

var Schema = mongoose.Schema;

const messageSchema = new Schema({
    fromUserSendID: {
        type: String,
        required: true
    },
    toUserIsRecivedID: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    dateSend: {
        type: Date,
        required: true
    },
    dateRead: {
        type: Date
    },
    status: {
        type: String
    }
});

const MessageSchema = mongoose.model('message', messageSchema);
module.exports = MessageSchema;