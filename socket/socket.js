var socketService = require('./../service/socket.service');
var _ = require('underscore');
var jwt = require('./../utils/jwt');
var user = require('./../models/user.model');

module.exports = function (io) {
    _.each(io.nsps, function (nsp) {
        nsp.on('connect', function (socket) {
            if (!socket.auth) {
                delete nsp.connected[socket.id];
            }
        });
    });

    var chat = io.of('chat');
    chat.on('connection', authenticationChat);

    function authenticationChat(socket) {
        socket.auth = false;
        socket.on('authentication', (token) => {
            console.log(socket.id);
            jwt.verify(token, (err, userModel) => {
                if (!err && userModel) {
                    socket.auth = true;
                    socket.user = userModel;

                    if (_.findWhere(chat.sockets, { id: socket.id }).id) {
                        chat.connected[userModel._id] = socket;
                    }
                    authenchatSuccess(chat, socket);
                } else {
                    socket.disconnect('AUTHEN_NOT_EXIST');
                }
            });
        });
    }

    function authenchatSuccess(chat, socket) {
        socket.on('make friend', (request) => {
            var recived = {
                sendRequestMakeFriendId: request.sendRequestMakeFriendId,
                idNeedMakeFriend: request.idNeedMakeFriend
            }
            console.log(recived);
            socketService.makeFriend(recived)
                .then(response => {
                    if (chat.connected[recived.idNeedMakeFriend]) {
                        var sokectIDURL = chat.connected[recived.idNeedMakeFriend].id;
                        console.log(49, sokectIDURL);
                        chat.to(sokectIDURL).emit('wait_to_accept', { result: response });
                    }
                }).catch(err => {
                    if (chat.connected[recived.idNeedMakeFriend]) {
                        var sokectIDURL = chat.connected[recived.idNeedMakeFriend].id;
                        chat.to(sokectIDURL).emit('wait_to_accept', { err: err });
                    }
                });
        });

        socket.on('sendMessage', (request) => {
            var recived = {
                idFriend: request.idfriend,
                idme: request.idme,
                dateSend: request.dateSend,
                message: request.message,
                image: request.image
            }
            socketService.sendMessage(recived)
            .then(result => {
                if(chat.connected[recived.idFriend]){
                    var sokectIDURL = chat.connected[recived.idFriend].id;
                    chat.to(sokectIDURL).emit('recivedMessage', { message: recived.message, dateSend: recived.dateSend });
                }
            }).catch(err => {
                console.log(err);
            });
        })

        socket.on('accept_user_make_friend', (request) => {
            var recived = {
                idusermakeFriend: request.idusermakeFriend,
                token: request.token
            }
            socketService.acceptUserMakeFriend(recived)
                .then(response => {
                    if (chat.connected[recived.idusermakeFriend]) {
                        var sokectIDURL = chat.connected[recived.idusermakeFriend].id;
                        chat.to(sokectIDURL).emit('you_are_accpeted_by_user_you_want_make_friend', { result: response });
                    } else {
                    }
                }).catch(err => {
                    chat.to(sokectIDURL).emit('you_are_not_accpeted_by_user_you_want_make_friend', { result: err });
                });
        });

        socket.on('disconnect', function () {
            //console.log('user has socket ID ', socket.user, ' disconnected');
        });
    }
}
