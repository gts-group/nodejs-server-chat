var router = require('express').Router();
var userController = require('./../controller/user.controller');
var auth = require('../middle-ware/auth');

module.exports = function () {
    router.get('/', auth.auth(), userController.getAllUser);
    router.get('/:email', auth.auth(), userController.getUserByEmail);
    router.get('/getuserbyid/:id', auth.auth(), userController.getUserById);
    router.get('/checkstateuser/:idother/:idme', auth.auth(), userController.checkStateUser);
    router.get('/checknotification/:idme', auth.auth(), userController.checkNotification);
    router.get('/canclefriend/:idother/:idme', auth.auth(), userController.cancleFriend);
    router.put('/:id', userController.updateUser);
    router.post('/searchUser', userController.searchUserArray);
    router.delete('/:id', auth.auth(), userController.deleteUser);
    router.post('/resgister', userController.createUser);
    router.post('/getmessage', userController.getMessage);
    router.post('/getarrayuserbyarrayid', auth.auth(), userController.getArrayUserByArrayId);
    router.post('/checktoken', auth.auth(), userController.checkTokenBrower);
    return router;
}