var router = require('express').Router();
var uploadfileController = require('./../controller/uploadfile.controller');
var auth = require('../middle-ware/auth');
module.exports = function () {
    router.put('/uploadfile/:id', auth.auth(), uploadfileController.uploadAvatar);
    router.put('/uploadimagestorage/:id', auth.auth(), uploadfileController.uploadImageStorage);
    router.post('/getimagestoragebyiD', auth.auth(), uploadfileController.getImageStorageByID)
    return router;
}